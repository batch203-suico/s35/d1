const express = require("express");
// Mongoose is a package that allows us to create Schemas to model our data structures and to manipulate our databases using different access methods.
const mongoose = require("mongoose");
const app = express();
const port = 3001;

// [SECTION] MongoDB Connection 
	// Syntax:
		/*
			mongoose.connect("<MongoDB Atlas 
			Connection Strings>", 
				{
					//Allows us to avoid any current and future errors while connecting to MongoDB
					useNewUrlParser:true,
					useUnifiedTopology: true
				}
			);
		*/
		mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.woo6ajr.mongodb.net/b203_to-do?retryWrites=true&w=majority",
				{
					
					useNewUrlParser:true,
					useUnifiedTopology: true
				}
			); 
			let db = mongoose.connection;

			db.on("error", console.error.bind(console, "connection error"));
			db.once("open", ()=> console.log("We're connected to the cloud database"));

			// [SECTION] Mongoose Schemas
				// Schemas determine the structure of documents to be written in the database
				// Schemas act as blueprints to our data
				// Syntax:
					/*
						const schemaName = new mongoose.Schema({<keyvalue:pair>});
					*/
				// name & status
				// "required" is used to specify that a field must not be empty.
				// "default" is used if a field value is not supplied
				const taskSchema = new mongoose.Schema({
					name: {
						type: String,
						required: [true, "Task name is required"]
					},
					status: {
						type: String,
						default: "pending"
					}
				}) 

				// [SECTION] Models
					// Uses schema and use it to create/instantiate documents/object that follows our schema structure.
					// The variable/object that will be created can be used to run commands for interacting with our database.
					// Syntax: const VariableName = mongoose.model("collectionName", schemaName);

					const Task = mongoose.model("Task", taskSchema);

				/*
                Mini Activity
                    1. Create a User schema.
                        username - string
                        password - string
                    2. Create a User model.
            	*/

            	const userSchema = new mongoose.Schema({
					username: {
						type: String,
						required: [true, "User name is required"]
					},
					password: {
						type: String,
						required: [true, "User name is required"]
					}
				})

				const User = mongoose.model("User", userSchema); 



// middlewares
app.use(express.json()); // allows app to read json data
app.use(express.urlencoded({extended:true})); // Allows you app to read data from forms.

// Business Logic
/*
    1. Add a functionality to check if there are duplicate tasks
        - If the task already exists in the database, we return an error message "Duplicate task found"
        - If the task doesn't exist in the database, we add it in the database
        A task does exists if:
            - result from the query is not null
            - result.name is equal to req.body.name
    2. The task data will be coming from the request's body
    3. Create a new Task object with a "name" field/property
    4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

	app.post("/tasks", (req, res) => {
		Task.findOne({name: req.body.name}, (err, result) =>{
			if(result !== null && result.name === req.body.name)
			{
				return res.send(`Duplicate task found!`);
			}
			else {
				// "newTask" = was created/instatiated form the Mongoose schema and will gain access to ".save" method
				let newTask = new Task({
					name: req.body.name
				});

				newTask.save((saveErr, savedTask) => {
					if(saveErr){
						return console.log(saveErr)
					}
					else{
						return res.status(201).send(`New task Created!`);
					}
				})
			}
		});
	});

	// Getting all the tasks

    // Business Logic
    /*
        1. Retrieve all the documents
        2. If an error is encountered, print the error
        3. If no errors are found, send a success status back to the client/Postman and return an array of documents
    */

    app.get("/tasks", (req,res) =>{
    	// Task.find({all documents in collection}, (err, result) =>{
		Task.find({name: "eating"}, (err, result) =>{
			if(err){
				return console.log(err);
			}
			else{
				return res.status(200).send({
					Tasks: result
				})
			}
		})
	})

	/*
			Instructions s35 Activity:
	1. Using User model.
	2. Create a POST route that will access the "/signup" route that will create a user.
	5. Process a POST request at the "/signup" route using postman to register a user.
	6. Create a git repository named S35.
	7. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	8. Add the link in Boodle.
	*/

	app.post("/signup", (req, res) => {
	
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, savedTask) => {
				if(saveErr){
					return console.log(saveErr)
				}
				else{
					return res.status(201).send(`New User Registered!`);
				}
			})
		
			
		
	});

	app.get("/signup", (req,res) =>{
		User.find({}, (err, result) =>{
			if(err){
				return console.log(err);
			}
			else{
				return res.status(200).send({
					Users: result
				})
			}
		})
	})



app.listen(port, () => console.log(`server is running at ${port}`));